<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2015 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Brian Barnes <brian.barnes@totaralearning.com>
 * @author Joby Harding <joby.harding@totaralearning.com>
 * @package theme_innovate
 */

defined('MOODLE_INTERNAL') || die;

use theme_basis\css_processor;

$component = 'theme_innovate';

if ($ADMIN->fulltree) {

    // -- SLIDESHOW

    // $ADMIN->add('themes', new admin_category($component, get_string('innovatesettings', $component)));

    $temp = new admin_settingpage($component . '_settings_slideshow', get_string('slideshow', $component . ''));

    // Enable searchbox 

    $name = "{$component}/slideshow";
    $title = new lang_string('enableslideshow', $component);
    $description = new lang_string('enableslideshowdesc', $component);
    $default = '1';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = $component . '/slidesamount';
    $title = get_string('slidesamount' , $component . '');
    $description = get_string('slidesamountdesc', $component . '');
    $default = '0';
    $choices = array(
      '0' => 'Disable slideshow',
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4',
      '5' => '5',
      '6' => '6',
      '7' => '7',
      '8' => '8',
      '9' => '9',
      '10' => '10'
      );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    if (!empty($PAGE->theme->settings->slidesamount)) {
      if ($PAGE->theme->settings->slidesamount > 0) {
        for ($i = 1; $i <= $PAGE->theme->settings->slidesamount; $i++) {
                
          $name = $component . '/slide'.$i;
          $title = get_string('slide'.$i, $component . '');
          $description = get_string('slide'.$i.'desc', $component . '');
          $setting = new admin_setting_configstoredfile($name, $title, $description, 'slide'.$i);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

          $name = $component . '/slide'.$i.'title';
          $title = get_string('slide'.$i.'title', $component . '');
          $description = get_string('slide'.$i.'titledesc', $component . '');
          $default = '';
          $setting = new admin_setting_configtext($name, $title, $description, $default);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

          $name = $component . '/slide'.$i.'text';
          $title = get_string('slide'.$i.'text', $component . '');
          $description = get_string('slide'.$i.'textdesc', $component . '');
          $default = '';
          $setting = new admin_setting_configtext($name, $title, $description, $default);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

          $name = $component . '/slide'.$i.'link';
          $title = get_string('slide'.$i.'link', $component . '');
          $description = get_string('slide'.$i.'linkdesc', $component . '');
          $default = '';
          $setting = new admin_setting_configtext($name, $title, $description, $default);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

          $name = $component . '/slide'.$i.'button';
          $title = get_string('slide'.$i.'button', $component . '');
          $description = get_string('slide'.$i.'buttondesc', $component . '');
          $default = '';
          $setting = new admin_setting_configtext($name, $title, $description, $default);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

        }
      }
    }

    // Slider title color
    $name = "{$component}/slidertitlecolor";
    $title = get_string('slidertitlecolor', $component);
    $description = get_string('slidertitlecolordesc', $component);
    $default = "#FFFFFF";
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Slider text color
    $name = "{$component}/slidertextcolor";
    $title = get_string('slidertextcolor', $component);
    $description = get_string('slidertextcolordesc', $component);
    $default = "#FFFFFF";
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $ADMIN->add($component . '', $temp);

}
