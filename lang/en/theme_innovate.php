<?php
$string['pluginname'] = 'Innovate';
$string['choosereadme'] = 'Innovate is a custom theme for Totara.';
 
// Block positions.
$string['region-bottom'] = 'Bottom';
$string['region-main'] = 'Main';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['region-top'] = 'Top';


$string['general'] = 'General';
$string['enablestyleoverrides'] = 'Enable style overrides';
$string['enablestyleoverrides_desc'] = 'Allow theme colour settings to override theme defaults.';

// -- Slideshow

$string['enableslideshow'] = 'Enable Slideshow';
$string['enableslideshowdesc'] = 'Enables the slideshow';

$string['slideshow'] = 'Slideshow';

$string['slidesamount'] = 'Amount of slides';
$string['slidesamountdesc'] = 'Amount of slides within the slideshow';

for ($i=1; $i < 11 ; $i++) {
	$string['slide'.$i] = 'Slide Background Image';
	$string['slide'.$i.'desc'] = 'Add an image to your slideshow, please make sure to make your pictures are the same size for better visual effect';

	$string['slide'.$i.'title'] = 'Slide Title';
	$string['slide'.$i.'titledesc'] = 'Title of the slide will display as a heading';

	$string['slide'.$i.'text'] = 'Slide Text';
	$string['slide'.$i.'textdesc'] = 'Text of the side will display as smaller text';

	$string['slide'.$i.'link'] = 'Slide Link';
	$string['slide'.$i.'linkdesc'] = 'Link to another page internally or externally';

	$string['slide'.$i.'button'] = 'Slide Button Text';
	$string['slide'.$i.'buttondesc'] = 'Text to be displayed on the button itself';
}

$string['slidertitlecolor'] = 'Slider text background colour';
$string['slidertitlecolordesc'] = 'This sets the slider text background colour for the theme.';
$string['slidertextcolor'] = 'Slider text colour';
$string['slidertextcolordesc'] = 'This sets the slider text colour for the theme.';

$string['login_username'] = 'username';
$string['login_password'] = 'password';
