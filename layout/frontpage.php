<?php
/*
 * This file is part of Totara LMS
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright 2015 Bas Brands <www.sonsbeekmedia.nl>
 * @author    Bas Brands
 * @author    David Scotson
 * @author    Joby Harding <joby.harding@totaralearning.com>
 * @author    Petr Skoda <petr.skoda@totaralms.com>
 * @author    Murali Nair <murali.nair@totaralearning.com>
 * @package   theme_legacy
 */

defined('MOODLE_INTERNAL') || die();

$PAGE->set_popup_notification_allowed(false);

$themerenderer = $PAGE->get_renderer('theme_legacy');
$full_header = $themerenderer->full_header();
if (isset($PAGE->layout_options['nonavbar']) && $PAGE->layout_options['nonavbar'] && strpos($full_header, '<input') === false) {
    $full_header = '';
}
echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<?php require(__DIR__."/partials/head.php"); ?>

<body <?php echo $OUTPUT->body_attributes(['legacy']); ?>>
<?php echo $OUTPUT->standard_top_of_body_html() ?>

<!-- Main navigation -->
<?php
$totara_core_renderer = $PAGE->get_renderer('totara_core');
$hasguestlangmenu = (!isset($PAGE->layout_options['langmenu']) || $PAGE->layout_options['langmenu'] );
$nocustommenu = !empty($PAGE->layout_options['nocustommenu']);
echo $totara_core_renderer->masthead($hasguestlangmenu, $nocustommenu);
?>

<?php
    if (isset($PAGE->theme->settings->slideshow)&&($PAGE->theme->settings->slideshow > 0) ) {
        require("{$CFG->dirroot}/theme/innovate/layout/partials/slider.php");
    }
?>
<img style="width: 101%;" src="<?php echo $CFG->wwwroot.'/theme/'. $PAGE->theme->name .'/pix/bottom_header.png';?>">

<div class="three-col-section">
    <div class="container-fluid">
        <div class="three-col-section-container-top">
            <div class="three-col-section-title">eLearning platforms</div>
            <div class="three-col-section-desc">Venenatis lectus magna fringilla urna porttitor rhoncus. Malesuada fames ac turpis egestas maecenas pharetra convallis posuere. </div>
        </div>
        <div class="three-columns-section-container-bottom">
            <div class="three-col-section-container-bottom bottom-l">
                <div class="three-col-section-container-bottom-img">
                    <img src="<?php echo $CFG->wwwroot.'/theme/'. $PAGE->theme->name .'/pix/4.png';?>">
                </div>
                <div class="three-col-section-container-bottom-title">Consectetur adipiscing elit, sed do eiusmod tempor incididunt.</div>
                <div class="three-col-section-container-bottom-desc">Tellus orci ac auctor augue. Aliquet porttitor lacus luctus accumsan. Eleifend donec pretium vulputate sapien nec sagittis. Semper quis lectus nulla at volutpat diam ut venenatis tellus.</div>
            </div>
            <div class="three-col-section-container-bottom bottom-c">
                <div class="three-col-section-container-bottom-img">
                    <img src="<?php echo $CFG->wwwroot.'/theme/'. $PAGE->theme->name .'/pix/5.png';?>">
                </div>
                <div class="three-col-section-container-bottom-title">Consectetur adipiscing elit, sed do eiusmod tempor incididunt.</div>
                <div class="three-col-section-container-bottom-desc">Tellus orci ac auctor augue. Aliquet porttitor lacus luctus accumsan. Eleifend donec pretium vulputate sapien nec sagittis. Semper quis lectus nulla at volutpat diam ut venenatis tellus.</div>
            </div>
            <div class="three-col-section-container-bottom bottom-r">
                <div class="three-col-section-container-bottom-img">
                    <img src="<?php echo $CFG->wwwroot.'/theme/'. $PAGE->theme->name .'/pix/6.png';?>">
                </div>
                <div class="three-col-section-container-bottom-title">Consectetur adipiscing elit, sed do eiusmod tempor incididunt.</div>
                <div class="three-col-section-container-bottom-desc">Tellus orci ac auctor augue. Aliquet porttitor lacus luctus accumsan. Eleifend donec pretium vulputate sapien nec sagittis. Semper quis lectus nulla at volutpat diam ut venenatis tellus.</div>
            </div>
        </div>
    </div>
</div>

<div class="three-col-section three-col-section-green">
    <div class="container-fluid">
        <div class="three-col-section-container-top">
            <div class="three-col-section-title">Popular Courses</div>
        </div>
        <div class="three-columns-section-container-bottom">
            <div class="three-col-section-container-bottom bottom-l">
                <div class="three-col-section-container-bottom-img">
                    <img src="<?php echo $CFG->wwwroot.'/theme/'. $PAGE->theme->name .'/pix/header.jpg';?>">
                </div>
                <div class="three-col-section-container-bottom-title">Consectetur adipiscing elit, sed do eiusmod tempor incididunt.</div>
                <div class="three-col-section-container-bottom-desc">Tellus orci ac auctor augue. Aliquet porttitor lacus luctus accumsan. Eleifend donec pretium vulputate sapien nec sagittis. Semper quis lectus nulla at volutpat diam ut venenatis tellus.</div>
                <a href="" class="three-col-section-container-bottom-desc">More details</a>
            </div>
            <div class="three-col-section-container-bottom bottom-c">
                <div class="three-col-section-container-bottom-img">
                    <img src="<?php echo $CFG->wwwroot.'/theme/'. $PAGE->theme->name .'/pix/header.jpg';?>">
                </div>
                <div class="three-col-section-container-bottom-title">Consectetur adipiscing elit, sed do eiusmod tempor incididunt.</div>
                <div class="three-col-section-container-bottom-desc">Tellus orci ac auctor augue. Aliquet porttitor lacus luctus accumsan. Eleifend donec pretium vulputate sapien nec sagittis. Semper quis lectus nulla at volutpat diam ut venenatis tellus.</div>
                <a href="" class="three-col-section-container-bottom-desc">More details</a>
            </div>
            <div class="three-col-section-container-bottom bottom-r">
                <div class="three-col-section-container-bottom-img">
                    <img src="<?php echo $CFG->wwwroot.'/theme/'. $PAGE->theme->name .'/pix/header.jpg';?>">
                </div>
                <div class="three-col-section-container-bottom-title">Consectetur adipiscing elit, sed do eiusmod tempor incididunt.</div>
                <div class="three-col-section-container-bottom-desc">Tellus orci ac auctor augue. Aliquet porttitor lacus luctus accumsan. Eleifend donec pretium vulputate sapien nec sagittis. Semper quis lectus nulla at volutpat diam ut venenatis tellus.</div>
                <a href="" class="three-col-section-container-bottom-desc">More details</a>
            </div>
        </div>
    </div>
</div>

<div class="two-col-section two-col-section-first">
    <div class="container-fluid">
        <div class="two-col-section-left"></div>
        <div class="two-col-section-right">
            <div class="two-col-section-title">What we do?</div>
            <div class="two-col-section-list">
                <div class="two-col-section-list-element">
                    <i class="fa fa-check" aria-hidden="true"></i>
                    <div class="two-col-section-list-element-text">Consectetur adipiscing elit, sed do eiusmod tempor incididunt.</div>
                </div>
                <div class="two-col-section-list-element">
                    <i class="fa fa-check" aria-hidden="true"></i>
                    <div class="two-col-section-list-element-text">Consectetur adipiscing elit, sed do eiusmod tempor incididunt.</div>
                </div>
                <div class="two-col-section-list-element">
                    <i class="fa fa-check" aria-hidden="true"></i>
                    <div class="two-col-section-list-element-text">Consectetur adipiscing elit, sed do eiusmod tempor incididunt.</div>
                </div>
                <div class="two-col-section-list-element">
                    <i class="fa fa-check" aria-hidden="true"></i>
                    <div class="two-col-section-list-element-text">Consectetur adipiscing elit, sed do eiusmod tempor incididunt.</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="two-col-section two-col-section-second">
    <div class="container-fluid">
        <div class="two-col-section-left">
            <div class="two-col-section-title">Our mission</div>
            <div class="two-col-section-subtitle">Consectetur adipiscing elit, sed do eiusmod tempor incididunt.</div>
            <div class="two-col-section-desc">Tellus orci ac auctor augue. Aliquet porttitor lacus luctus accumsan. Eleifend donec pretium vulputate sapien nec sagittis. Semper quis lectus nulla at volutpat diam ut venenatis tellus.</div>
            <button href="#" class="two-col-section-btn btn">Sample button</button>
        </div>
        <div class="two-col-section-right"></div>
    </div>
</div>

<?php if ($full_header !== '') { ?>
<!-- Breadcrumb and edit buttons -->
<div class="container-fluid breadcrumb-container">
    <div class="row">
        <div class="col-sm-12">
            <?php echo $full_header; ?>
        </div>
    </div>
</div>
<?php } ?>

<!-- Content -->
<div id="page" class="container-fluid">
    <div id="page-content">

        <?php echo $themerenderer->blocks_top(); ?>
        <div class="row">
            <div id="region-main" class="<?php echo $themerenderer->main_content_classes(); ?>">
                <?php echo $themerenderer->course_content_header(); ?>
                <?php echo $themerenderer->main_content(); ?>
                <?php echo $themerenderer->blocks_main(); ?>
                <?php echo $themerenderer->course_content_footer(); ?>
            </div>
            <?php echo $themerenderer->blocks_pre(); ?>
            <?php echo $themerenderer->blocks_post(); ?>
        </div>
        <?php echo $themerenderer->blocks_bottom(); ?>

    </div>
</div>

<!-- Footer -->
<?php require(__DIR__."/partials/footer.php"); ?>
<?php require("{$CFG->dirroot}/theme/innovate/layout/partials/slideroptions.php"); ?>
<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>
