<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @copyright 2016 onwards Totara Learning Solutions LTD
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package   theme_innovate
 */

defined('MOODLE_INTERNAL') || die();
?>


<script type="text/javascript"> var jQ = jQuery.noConflict(); </script>

<script type="text/javascript" src="<?php echo $CFG->wwwroot.'/theme/'.$PAGE->theme->name.'/javascript/jquery.flexslider.js';?>"></script>

<script type="text/javascript">
        // -- slideshow init
        console.log('JQ');
        jQ('.flexslider').flexslider({
            animation: "fade",
            animationSpeed: 600
        });

</script>