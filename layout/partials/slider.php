<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @copyright 2016 onwards Totara Learning Solutions LTD
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package   theme_innovate
 */

defined('MOODLE_INTERNAL') || die();
?>

<section class="slider">
    <div class="flexslider">

        <ul class="slides">
            <?php
            if (isset($PAGE->theme->settings->slidesamount)&&($PAGE->theme->settings->slidesamount > 0) ) {
                 
                for ($i=1; $i <= $PAGE->theme->settings->slidesamount ; $i++) {
                    if((empty($PAGE->theme->settings->{'slide'.$i.'title'})) && (empty($PAGE->theme->settings->{'slide'.$i.'text'})) && (empty($PAGE->theme->settings->{'slide'.$i.'button'}))){
                        $slidelink = ($PAGE->theme->settings->{'slide'.$i.'link'});
                        echo "<li>";
                            echo "<a href='".$slidelink."'>";
                            echo '<img src="'.$PAGE->theme->setting_file_url('slide'.$i,'slide'.$i).'">';
                            echo "</a>";
                        echo "</li>";
                    }
                    else {
                        echo "<li>";
                        echo '<img src="'.$PAGE->theme->setting_file_url('slide'.$i,'slide'.$i).'">';

                        if (!empty($PAGE->theme->settings->{'slide'.$i.'title'})) {
                            if (($PAGE->theme->settings->{'slide'.$i.'title'}) !="") {

                                $slidetitle = ($PAGE->theme->settings->{'slide'.$i.'title'});
                                $slidetext = ($PAGE->theme->settings->{'slide'.$i.'text'});
                                $slidelink = ($PAGE->theme->settings->{'slide'.$i.'link'});
                                $slidebuttontext = ($PAGE->theme->settings->{'slide'.$i.'button'});

                                echo "<div id='slide-info' class='media-bg'>";
                                    echo "<div id='slide-info-wrapper'>";
                                        echo "<p class='slide-title'>".$slidetitle."</p>";
                                        echo "<div class='slide-text-bg'><p class='slide-text'>".$slidetext."</p></div>"; 
                                        echo "<a class='btn' href='".$slidelink."' class='slide-has-link'>".$slidebuttontext."</a>";
                                    echo "</div>";
                                echo "</div>";
                        }}                            
                    
                        echo "</li>";
                    }
                }
            }?>
        </ul>

    </div>
</section>

