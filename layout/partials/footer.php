<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @copyright 2016 onwards Totara Learning Solutions LTD
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package   theme_legacy
 */

defined('MOODLE_INTERNAL') || die();

?>

<!-- Scroll to top button  -->
<div class="scrollBtn">
    <i class="fa fa-chevron-up" aria-hidden="true"></i>
</div>
<script type="text/javascript">
        // -- scroll top btn
        $(window).scroll(function(){

            // -- scroll top btn
            if ($(this).scrollTop() > 100) {
                $('.scrollBtn').fadeIn();
            } else {
                $('.scrollBtn').fadeOut();
            }

            if ($(this).scrollTop() > 10) {
                $('#has-logo').addClass('has-logo-scroll');
            } else {
                $('#has-logo').removeClass('has-logo-scroll');
            }

        });
        
        $('.scrollBtn').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });
</script>

<footer id="page-footer" class="page-footer">
    <div class="container-fluid">
        <div class="footer-container-left">
            <img src="<?php echo $CFG->wwwroot.'/theme/'. $PAGE->theme->name .'/pix/footer_logo.svg';?>">
            <div class="footer-container-left-text">© 2020 Innovate Learn Ltd</div>
        </div>
        <div class="footer-container-right">
            <div class="footer-container-right-text"><a href="#">Privacy Policy</a></div>
        </div>
    </div>
</footer>
