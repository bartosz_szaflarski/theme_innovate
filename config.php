<?php
$THEME->doctype = 'html5';
$THEME->name = 'innovate';
$THEME->parents = ['legacy', 'base'];
$THEME->enable_dock = true;
$THEME->enable_hide = true;
$THEME->minify_css = false;

$THEME->layouts = array(
    // Main course page.
    'course' => array(
        'file' => 'course.php',
        'regions' => array('top', 'bottom', 'side-pre', 'side-post'),
        'defaultregion' => 'side-pre',
        'options' => array('langmenu' => true),
    ),
    // The site home page.
    'frontpage' => array(
        'file' => 'frontpage.php',
        'regions' => array('top', 'main', 'bottom', 'side-pre', 'side-post'),
        'defaultregion' => 'side-pre',
        'options' => array('nonavbar' => true),
    ),
    'login' => array(
    'file' => 'login.php',
    'regions' => array(),
    'options' => array('langmenu' => true, 'nototaramenu' => true, 'nonavbar' => true),
    ),
);
